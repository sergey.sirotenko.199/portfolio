const express = require("express");
const expressHbs = require("express-handlebars");
const hbs = require("hbs");
const MongoClient = require("mongodb").MongoClient;
const language = require('./modules/languages/languages');

const mongoClient = new MongoClient("mongodb://localhost:27017/", { useNewUrlParser: true });
const app = express();


hbs.registerPartials(__dirname + "/views/partials");

app.use('/assets',express.static(__dirname +'/assets'));
app.use('/assets/js/jquery.min.js',express.static(__dirname +'/node_modules/jquery/dist/jquery.min.js'));

app.engine("hbs", expressHbs(
    {
        layoutsDir: "views/layouts", 
        defaultLayout: "default",
        extname: "hbs"
    }
));
app.set("view engine", "hbs");
hbs.registerPartials(__dirname + "/views/partials");


app.get("/", function(request, response){
    response.render("home.hbs");
});
app.get("/home", function(request, response){
    response.render("home.hbs");
});
app.get("/my-location", function(request, response){
    response.render("myLocation.hbs");
});
app.get("/my-works", function(request, response){
    response.render("myWorks.hbs");
});


app.get("/rewiews", function(request, response){
    console.log(request.headers.host);
    mongoClient.connect(function(err, client){
        const db = client.db("portfolio_db");

        const reviews = db.collection("reviews");
        var cursor = reviews.find();

        cursor.toArray(function(err, res){
            response.render("rewiews.hbs", {
                reviews: res
            });
        });
        client.close();
    });
});

app.listen(4012);