
let Gallery = function(wrapper, items) {
    this.wrapper = wrapper;
    this.items = items;
    this.activeIndex = 0;
    this.render();
    var timeStamp = 0.0;
    $(window).bind('mousewheel',(e) => {
        if((e.originalEvent.timeStamp - timeStamp) > 500) {
            timeStamp = e.originalEvent.timeStamp;
            this.next();
        }
    });
}
Gallery.prototype = {
    links: [],
    render: function() {
        self = this;
        this.showAhimation(this.items.first());
        let galleryWrapper = $(document.createElement("div")).addClass("gallery-nav-wrapper")
        this.items.each(function(index, e){
            let link = $(document.createElement('div')).addClass("gallery-link").data("gallery-link-id", index);
            self.links.push(link);
            if(index === 0) link.addClass('active');
            self.gallerySitebar.push({
                DOM_element: e,
                link: link
            });
            galleryWrapper.append(link);
            link.on('click', (e) => {
                self.items.each((index, e) => {
                    if($(e).hasClass('active')) {
                        $(e).css({'display':'none'}).removeClass('active');
                        $(self.links[index]).removeClass('active');
                    }
                });
                self.showAhimation($(self.items[index]));
                $(e.target).addClass('active');
            })
        });  
        this.wrapper.append(galleryWrapper);
    },
    gallerySitebar: [],
    next: function() {
        self = this;
        this.items.each(function(index, e){
            if($(e).is(":visible")) {
                $(self.links[index]).removeClass('active');
                $(e).css({display:'none'}).removeClass('active');
                if(index < self.items.length - 1){
                    self.showAhimation($(self.items[index + 1]));
                    $(self.links[index + 1]).addClass('active');
                }else{
                    console.log('end');
                    self.showAhimation($(self.items[0]));
                    $(self.links[0]).addClass('active');
                }
                return false;
            }
        });
    },
    showAhimation(element) {
        element.css({
            top: -65,
            opacity: 0,
            display: 'block'
        }).animate({
            top: 0,
            opacity: 1
        }, 420).addClass('active');
        element.find('.block-title').css({
            top: -70
        }).animate({
            top: -50
        }, 520);
    }
}   