$(document).ready(function(){

    let windowwidth = window.screen.width;
    let mobileWidht = 800;
    let isMobile =  window.screen.width < 800;

    let readyRender = function(){
        $('#preloader').animate({
            opacity: 0
        }, 200, function(){
            let gallery = new Gallery($(".gallery-wrapper"), $(".gallery-wrapper .gallery-item"))
        });
    }

    if(!isMobile) readyRender();

    let hideWindow = function(window) {
        window.animate({
            top: -30,
            opacity: 0
        }, 300, function(){
            $(this).closest('.modal-wrapper').css({
                display: 'none'
            });
            /*$(this).css({
                top: 0,
                opacity: 1
            });*/
        });
    } 

    $('.close-modal').on('click', function(){
        hideWindow($(this).closest('.modal-inner'));
    });

    var showModal = function(modal) {
        modal.css({
            display: 'flex'
        }).find('.modal-inner').animate({
            top: 0,
            opacity: 1
        }, 200);
    }

    window.showFeedbackForm = function() {
        showModal($('#feedback'));
    }

    window.showFullRewiew = function(_id) {
        $.get('/rewiews/'+_id, function(response){
            let tpl = `
            <div class='full-review'>
                <div class='full-review_img'>
                    <img src="${response.img}" />
                </div>
                <h2 class='full-review_title'>${response.title}</h2>
                <p class='full-review_content'>${response.reviewBody}</p>
            </div>`;
            $('#full-review-wrapper').find(".full-review-content").html(tpl);
            showModal($('#full-review-wrapper'));
        });
    }

    $('.btn-action').on('click', function(){
        let action = $(this).data('action');
        window[action.method](...action.params);
    });

    //===LANG*SELECT===
    let langFormHide = function(form) {
        form.fadeOut(200).removeClass('visible');
        form.children("p").each(function(index,e) {
            $(e).animate({
                top: 0
            }, 300);
        });
    }
    let langFormShow = function(form) {
        form.fadeIn(200).addClass('visible');
        form.children("p").each(function(index,e) {
            $(e).animate({
                top: index * 24
            }, 300);
        });
    }
    $(".language-select-wrapper > .selected-language").on('click', function() {
        let form = $(".language-values");
        if(form.hasClass('visible')) {
            langFormHide(form);
        }else{
            langFormShow(form);
        }
    });
    $(".language-values > p").on('click', function(){
        let lang = $(this).text();
        $(".language-select-wrapper > .selected-language").text(lang);
        let form = $(".language-values");   
        location.host = lang + ".ssirotenko.ru";
        langFormHide(form);
    });

    $(".feedback-form").on('submit', function(e) {
        e.preventDefault();
        var feedback = new DataParser(e.target);
        $.post('/feebback',feedback.data, function(res) {
            if(res.status == 200 && res.smtpSendSuccess) {
                let message = new Message(res.message);
                feedback.clear();
                hideWindow($(e.target).closest(".modal-inner"))
            }
        });
    });

    $(".sitebar-toggle_btn").on("click", function(){
        $(this).closest(".sitebar-wrapper").toggleClass("visible");
    })

});


//=====fb
var DataParser = function(form) {
    this.form = form;
    this.serialize();
}
DataParser.prototype = {
    serialize: function() {
        this.formInputs = this.form.getElementsByTagName('input');
        let obj = new Object();
        for(let i = 0; i < this.formInputs.length; i++ ) {
            let val = this.formInputs[i].value;
            let name = this.formInputs[i].name;
            if(val && name) obj[name] = val;
        }
        this.data = obj;
    },
    clear: function() {
        for(let i = 0; i < this.formInputs.length; i++ ) {
            this.formInputs[i].value = "";
        }
    }
}

var Message = function(messageText, type = 'success') {
    this.messageText = messageText;
    this.type = type;
    this.show();
}
Message.prototype = {
    show: function() {
        let message = this.renderHTML(this.messageText);
        $('body').append(message.tpl);
        setTimeout(function(){
            $("body").find("#num-"+message._id).animate({
                top: 45,
                opacity: 0
            }, 200, function() {
                $(this).remove();
            })
        }, 4000);
    },
    renderHTML: function(text) {
        let _id = Math.round(Math.random() * 10000);
        return {
            tpl: `
            <div class='message-wrapper' id='num-${_id}'>
                <p>${text}</p>
            </div>`,
            _id: _id
        }
    }
}