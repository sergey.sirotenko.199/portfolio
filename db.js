var MongoClient = require('mongodb').MongoClient;

var state = {
    db: null
}

module.exports.connect = function(url, done) {
    if(state.db) {
        return done();
    }
    MongoClient.connect(url, { useNewUrlParser: true }, function(err, client) {
        if(err) {
            return done();
        }
        state.db = client.db("portfolio_db");
        done();
    });
} 


module.exports.get = function() {
    return state.db;
}