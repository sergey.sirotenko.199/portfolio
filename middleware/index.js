module.exports = function(app, express) {

    const hbs = require("hbs");
    const expressHbs = require("express-handlebars");

    hbs.registerPartials(__dirname + "/views/partials");
    app.engine("hbs", expressHbs(
        {
            layoutsDir: "views/layouts", 
            defaultLayout: "default",
            extname: "hbs"
        }
    ));
    app.set("view engine", "hbs");
    hbs.registerPartials(__dirname + "/views/partials");
}
