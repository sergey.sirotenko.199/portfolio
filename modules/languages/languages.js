const fs = require('fs');
const langDir = "./modules/languages/list/";
const LangListDir = "./list/";
let lenguages = [];


module.exports.list = [];
module.exports.getLangContent = function(langName) {
    return require(LangListDir+langName+".json")
}

module.exports.getLangDataFromDomain = function(domain) {
    let subDomain = domain.split(".")[0];
    return lenguages.find(x => x == subDomain) ? lenguages.find(x => x == subDomain) : "ru";
} 

fs.readdirSync(langDir).forEach(file => {
    module.exports.list.push({
        name: file.split('.')[0],
        fileName: file,
    });
    lenguages.push(file.split('.')[0]);
});
