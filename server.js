const express = require("express");
const language = require('./modules/languages/languages');
const app = express();
const MongoClient = require("mongodb").MongoClient;
const mongoClient = new MongoClient();
const middleware = require('./middleware')(app, express);
const mailer = require("nodemailer");
const ObjectID  = require('mongodb').ObjectID 


const db = require("./db");

var viewData = {
    languages: language.list,
    selectedLanguage: 'ru'
}

app.use(express.urlencoded());
app.use(express.json());
app.use('/assets',express.static(__dirname +'/assets'));
app.use('/assets/js/jquery.min.js',express.static(__dirname +'/node_modules/jquery/dist/jquery.min.js'));

var renderResult = function(request, response, viewName, obj) {
    let lang = language.getLangDataFromDomain(request.headers.host);
    let pathname = request._parsedUrl.pathname;
    db.connect("mongodb://localhost:27017/", function() {
        db.get().collection("links").find({
            'language': lang
        }).toArray(function(err, res) {
            if(err) throw err;
            db.get().collection("meta").findOne({
                'language': lang,
                'page': pathname
            }, function(err, result){
                if (err) throw err;
                obj.languages = language.list;
                obj.selectedLanguage = lang;
                obj.words = language.getLangContent(obj.selectedLanguage);
                obj.links = res;
                obj.meta = result;
                response.render(viewName, obj);
            });
        });
    });
}

let renderHomePage = function(request, response) {
    let lang =  language.getLangDataFromDomain(request.headers.host);
    db.connect("mongodb://localhost:27017/", function() {
        db.get().collection("content").find({
            'language': lang
        }).toArray(function(err, res) {
            if(err) throw err;
            renderResult(request,response,"home.hbs",{
                content: res
            });
        });
    });
}
let renderReviewsPage = function(request, response) {
    let lang =  language.getLangDataFromDomain(request.headers.host);
    db.connect("mongodb://localhost:27017/", function() {
        db.get().collection("reviews").find({
            'language': lang
        }).toArray(function(err, res) {
            if(err) throw err;
            renderResult(request,response,"reviews.hbs",{
                reviews: res
            });
        });
    }); 
}
let renderMyWorksPage = function(request, response) {
    let lang =  language.getLangDataFromDomain(request.headers.host);
    db.connect("mongodb://localhost:27017/", function() {
        var skills = [];
        db.get().collection("userSkills").find().toArray(function(err, res) {
            if(err) throw err;
            skills = res.filter(x => x.parent == null);
            skills.forEach(e => {
                e.childs = res.filter(x => x.parent == e._id.toString());
            });   
            db.get().collection("works").find({
                'language': lang
            }).toArray(function(err, res) {
                if(err) throw err;
                renderResult(request,response,"myWorks.hbs",{
                    skills: skills,
                    works: res
                });
            });
        });   
    });    
}
let renderMyLocationPage = function(request, response) {
    renderResult(request,response,"myLocation.hbs",{});
}

let feebbackAction = function(request, response) {
    let smtpTransport = mailer.createTransport({ 
        service: "gmail",
        host: "smtp.gmail.com",
        auth: {
            user: "sergey.sirotenko.199@gmail.com",
            pass: "Corsar28"
        }
    });
    let mail = {
        from: "Sergey Sirotenko <sergey.sirotenko.199@gmail.com>",
        to: "sergey.sirotenko.199@gmail.com",
        subject: "New user on my site",
        html: 
        `<table style='border: 1px solid #111; padding: 5px;'>
            <tbody>
                <tr>
                    <td><b>NAME: </b></td>
                    <td>${request.body.username}</td>
                </tr>
                <tr>
                    <td><b>E-MAIL: </b></td>
                    <td>${request.body.email}</td>
                </tr>
                <tr>
                    <td><b>COUNTRY: </b></td>
                    <td>${request.body.country}</td>
                </tr>
            </tbody>
        </table>`
    }
    smtpTransport.sendMail(mail, function(error, response){
        if(error) console.log(error);
        smtpTransport.close();
    });
    response.send({
        status: 200,
        message: `Thank you for the application ${request.body.username}. I will contact you shortly.`,
        smtpSendSuccess: true
    });
}
let getReview = function(request, response) {
    db.connect("mongodb://localhost:27017/", function() {
        db.get().collection("reviews").findOne({
            _id: ObjectID(request.params._id)
        }, function(err, result){
            if (err) throw err;
            response.send(result);
        });
    });    
}

app.get("/", renderHomePage);
app.get("/home", renderHomePage);
app.get("/my-location", renderMyLocationPage);
app.get("/my-works", renderMyWorksPage);
app.get('/rewiews', renderReviewsPage);
app.get('/rewiews/:_id', getReview);

app.post('/feebback', feebbackAction)

app.listen(4012, function () {});